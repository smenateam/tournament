# -*- coding: utf-8 -*
from django.views.generic import ListView, TemplateView
from django.views.generic.edit import BaseDeleteView
from django.views import View

from django.http import HttpResponseRedirect, JsonResponse, HttpResponse

from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth import logout

from django.db.models import Min, Max, Case, When, Value, CharField

from django.urls import reverse_lazy

from .models import Tournament, Participant, Match
from .utils import get_group_counts, get_list_of_list

import random
import math
import datetime
import json


class JSONResponseMixin(object):

    def render_to_json_response(self, context, **response_kwargs):
        return JsonResponse(
            self.get_data(context), safe=False,
            **response_kwargs
        )

    def get_data(self, context):
        return context


class TournamentsView(ListView):
    template_name = u'tournament/tournaments.html'
    context_object_name = 'tournaments'
    queryset = Tournament.objects.annotate(
        early_date=Min('match__date'), last_date=Case(
            When(winner__isnull=False, then=Max('match__date')),
            default=Value(''),
            outuput_field=CharField()
        )
    ).order_by('early_date', '-id')


class LogoutView(View):

    def get(self, request):
        logout(request)
        return HttpResponseRedirect(request.GET.get('next'))


class TournamentDeleteView(
    UserPassesTestMixin, JSONResponseMixin, BaseDeleteView
):
    login_url = reverse_lazy('admin:login')
    model = Tournament

    def test_func(self):
        return self.request.user.is_superuser

    def get_object(self):
        try:
            obj = self.model.objects.get(id=self.kwargs['id'])
        except self.model.DoesNotExist:
            obj = None
        return obj

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object:
            self.object.delete()
        else:
            return HttpResponse(status=404)
        return HttpResponse(status=200)


class TournamentCreateTemplateView(ListView):
    template_name = 'tournament/tournament-create-form.html'
    context_object_name = 'participants'
    queryset = Participant.objects.all().order_by('name')


def create_round_robin_matches(participants, tournament, i=None):
    from competitions.scheduler import roundrobin
    scheduler = roundrobin.SingleRoundRobinScheduler(participants)
    for tour in scheduler.generate_schedule():
        for pair in tour:
            if None in pair:
                continue
            match = Match(tournament=tournament, participant1=pair[0],
                          participant2=pair[1])
            if i:
                match.group = i
            match.save()


def create_play_off_matches(participants, tournament):
    level = 0
    len_participants = len(participants)
    while level < math.log(len_participants, 2):
        random.shuffle(participants)
        matches = []
        for i in range(0, len(participants), 2):
            args = {
                'tournament': tournament,
                'level': level,
            }
            if level == 0:
                args.update({
                    'participant1': participants[i],
                    'participant2': participants[i+1],
                })
            else:
                args.update({
                    'prev_match1': participants[i],
                    'prev_match2': participants[i+1],
                })
            match = Match(**args)
            match.save()
            matches.append(match)
        level += 1
        if level > 0:
            participants = matches


def create_round_playoff_grouped(participants, tournament):
    participants_len = len(participants)
    group_counts = get_group_counts(participants_len)
    step_add = 0
    if participants_len % group_counts:
        step_add = 1
    step = participants_len / group_counts + step_add
    participants_grouped = get_list_of_list(participants, step)
    if len(participants_grouped[-1]) == 1:
        el = participants_grouped.pop()
        participants_grouped[-1].extend(el)
    for i, participants in enumerate(participants_grouped, start=1):
        create_round_robin_matches(participants, tournament, i)


def get_group_ids(ms):
    return list(set([m.group for m in ms]))


def get_grouped_tournament_table(ps, ms, group):
    ms = [m for m in ms if m.group == group]
    ps = get_participants(ms)
    return get_tournament_table(ps, ms)


def get_grouped_tables(ps, ms):
    tables = {}
    group_ids = get_group_ids(ms)
    groups = dict(Match.GROUP_CHOICES)
    for group_id in group_ids:
        group_name = groups[group_id]
        tables[group_name] = get_grouped_tournament_table(ps, ms)
    return tables


def get_count_of_ps_for_playoff(count):
    base = 2
    new_count = base ** (count.bit_length() - 1)
    if count != new_count or new_count == base:
        return new_count
    else:
        return get_count_of_ps_for_playoff(count - 1)


def get_group_count(matches):
    return len(set([match.group for match in matches if match.group]))


def get_min_participants_count_in_groups(matches):
    counts = {}
    for match in matches:
        if match.group:
            if match.group not in counts:
                counts[match.group] = {'count': 0, 'ps': []}
            counts[match.group]['ps'].append(match.participant1)
            counts[match.group]['ps'].append(match.participant2)
    for group, count_ps in counts.iteritems():
        count_ps['count'] = len(set(count_ps['ps']))
    return min([v['count'] for v in counts.values()])


def get_participants_for_playoff(tournament):
    ms = Match.objects.filter(tournament=tournament)
    ps = get_participants(ms)
    if tournament.is_subgroups:
        min_ps_in_group = get_min_participants_count_in_groups(ms)
        ps_len = min_ps_in_group * get_group_count(ms)
    else:
        ps_len = len(ps)
    count_of_ps_for_playoff = get_count_of_ps_for_playoff(ps_len)
    table = get_tournament_table(ps, ms)[:count_of_ps_for_playoff]
    return [p[1]['participant'] for p in table]


def create_matches(tournament):
    style = tournament['style_name']
    participants = tournament['participants_obj']
    is_subgroups = tournament['is_subgroups']
    tournament = tournament['tournament_obj']
    random.shuffle(participants)
    if style == Tournament.ROUND_ROBIN:
        create_round_robin_matches(participants, tournament)
    elif style == Tournament.PLAYOFF:
        create_play_off_matches(participants, tournament)
    elif style == Tournament.ROUND_PLAYOFF:
        if is_subgroups:
            create_round_playoff_grouped(participants, tournament)
        else:
            create_round_robin_matches(participants, tournament)


class TournamentCreateView(UserPassesTestMixin, View):
    login_url = reverse_lazy('admin:login')

    def test_func(self):
        return self.request.user.is_superuser

    def post(self, request, *args, **kwargs):
        tournament_data = json.loads(request.body)
        tournament = Tournament(
            name=tournament_data['name'], style=tournament_data['style'],
            is_subgroups=tournament_data['is_subgroups']
        )
        tournament.save()

        participants = []
        for participant in tournament_data['participants']:
            participant, _ = Participant.objects.get_or_create(
                name=participant)
            participants.append(participant)

        tournament_data['tournament_obj'] = tournament
        tournament_data['participants_obj'] = participants
        create_matches(tournament_data)

        return HttpResponse(status=200)


def get_splitted_result(result):
    dash = result.index('-')
    return result[:dash], result[dash+1:]


def is_draw(match):
    results = get_splitted_result(match.result)
    return match.result and results[0] == results[1]


def get_tournament_table(participants_qs, matches_qs):
    table = {}
    for p in participants_qs:
        if p is None:
            continue
        count = win_count = points = lose = goals_positive = goals_negative = 0
        for m in matches_qs:
            if m.result:
                if m.participant1 == p or m.participant2 == p:
                    count += 1
                    results = get_splitted_result(m.result)
                    goal_p1 = int(results[0])
                    goal_p2 = int(results[1])
                    if m.participant1 == p:
                        goals_positive += goal_p1
                        goals_negative += goal_p2
                    else:
                        goals_positive += goal_p2
                        goals_negative += goal_p1
                if m.winner and m.winner != p \
                        and (m.participant1 == p or m.participant2 == p):
                    lose += 1
            if m.winner == p:
                win_count += 1
                points += 3
            elif (m.participant1 == p or m.participant2 == p) and m.result \
                    and is_draw(m):
                points += 1
        tbl = {}
        tbl['count'] = count
        tbl['win_count'] = win_count
        tbl['points'] = points
        tbl['lose'] = lose
        sub_win_lose = goals_positive - goals_negative
        if sub_win_lose > 0:
            sub_win_lose = '+' + str(sub_win_lose)
        tbl['sub_win_lose'] = sub_win_lose
        tbl['participant'] = p
        table[p.name] = tbl
    table = sorted(table.items(), key=lambda item: item[0])
    return sorted(
        table,
        key=lambda item: (item[1]['points'], item[1]['sub_win_lose']),
        reverse=True
    )


def get_playoff_level(match):
    count = Match.objects.filter(
        tournament=match.tournament, level=match.level).count()
    return '1/%s' % count if count != 1 else 'Финал'


def get_matches_added_winner_level_wrap(matches, is_playoff=False):
    for match in matches:
        if match.result and is_draw(match):
            match.winner_wrap = 'Ничья'
        elif match.result is None:
            match.winner_wrap = ''
        else:
            match.winner_wrap = match.winner
        if is_playoff:
            match.level_wrap = get_playoff_level(match)
    if is_playoff:
        matches = [
            match for match in matches
            if match.participant1 is not None
            and match.participant2 is not None
        ]
    return matches


def get_participants(matches):
    ps = []
    for m in matches:
        if m.participant1:
            ps.append(m.participant1)
        if m.participant2:
            ps.append(m.participant2)

    return list(set(ps))


def get_tournament_winner(ms, ps, tournament_table, tournament_name):
    if tournament_name != Tournament.ROUND_ROBIN:
        try:
            if tournament_name == Tournament.PLAYOFF:
                count_of_ps_for_playoff = len(ps)
            else:
                count_of_ps_for_playoff = get_count_of_ps_for_playoff(len(ps))
            match = ms.get(level=math.log(count_of_ps_for_playoff, 2) - 1)
            if match.result:
                results = get_splitted_result(match.result)
                if results[0] > results[1]:
                    return match.participant1
                else:
                    return match.participant2
            else:
                return None
        except Match.DoesNotExist:
            return None
    else:
        if not is_round_tournament_over(ms):
            return None
        try:
            return tournament_table[0][1]['participant']
        except KeyError:
            return None


def is_round_tournament_over(ms):
    for m in ms:
        if not m.result:
            return False
    return True


def is_playoff_exist(matches):
    for match in matches:
        if match.level is not None:
            return True
    return False


class TournamentView(TemplateView):
    template_name = 'tournament/tournament.html'

    def get_context_data(self, **kwargs):
        context = super(TournamentView, self).get_context_data(**kwargs)
        tournament_id = self.kwargs['id']
        tournament = Tournament.objects.get(id=tournament_id)
        tournament_name = tournament.get_style_display()
        ms = Match.objects.filter(tournament=tournament_id)
        ps = get_participants(ms)
        ps_count = len(ps)
        tournament_table = get_tournament_table(ps, ms)
        if tournament_name != Tournament.PLAYOFF:
            context.update({
                'tournament_table': tournament_table,
            })
        if tournament_name == Tournament.ROUND_PLAYOFF and\
                not is_playoff_exist(ms) and is_round_tournament_over(ms):
            participants = get_participants_for_playoff(tournament)
            create_play_off_matches(participants, tournament)
        if tournament_name == Tournament.ROUND_PLAYOFF and\
                tournament.is_subgroups:
            tables_grouped = get_grouped_tables(ps, ms)
            context.update({'tournament_tables_grouped': tables_grouped})

        winner = get_tournament_winner(
            ms, ps, tournament_table, tournament_name
        )
        if winner:
            tournament.winner = winner
            tournament.save()
        context.update({
            'tournament': Tournament.objects.get(id=tournament_id),
            'matches': sorted(
                get_matches_added_winner_level_wrap(
                    Match.objects.filter(
                        tournament=tournament_id, level__isnull=True),
                ),
                key=lambda match:
                    match.date or datetime.date(datetime.MAXYEAR, 1, 1)
            ),
            'matches_playoff': sorted(
                get_matches_added_winner_level_wrap(
                    Match.objects.filter(
                        tournament=tournament_id, level__isnull=False),
                    is_playoff=True,
                ),
                key=lambda match:
                    match.date or datetime.date(datetime.MAXYEAR, 1, 1)
            ),
            'winner': winner,
            'tour_step': ps_count / 2,
        })
        return context


def set_match_winner(match):
    if not match.result:
        return
    results = get_splitted_result(match.result)
    if int(results[0]) > int(results[1]):
        match.winner = match.participant1
    elif int(results[0]) < int(results[1]):
        match.winner = match.participant2


def fill_next_level_matches(match):
    matches = Match.objects.filter(tournament=match.tournament)
    if match.level == math.log(len(get_participants(matches)), 2) - 1:
        return None
    matches_level_next = matches.filter(level=match.level+1)
    for m in matches_level_next:
        prev1 = m.prev_match1
        prev2 = m.prev_match2
        if (prev1 == match or prev2 == match) and prev1 and prev2:
            if prev1 == match:
                m.participant1 = match.winner
            elif prev2 == match:
                m.participant2 = match.winner
            m.save()


def is_next_level_matches_exist(match):
    return bool(Match.objects.filter(
        tournament=match.tournament, level__isnull=False))


def is_current_level_matches_played(match):
    matches = Match.objects.filter(
        tournament=match.tournament, level=match.level)
    for match in matches:
        if not match.result:
            return False
    return True


class MatchesUpdate(JSONResponseMixin, UserPassesTestMixin, View):
    login_url = reverse_lazy('admin:login')

    def test_func(self):
        return self.request.user.is_superuser

    def post(self, request, *args, **kwargs):
        matches_data = json.loads(request.body)
        for match_data in matches_data:
            match = Match.objects.get(id=match_data['id'])
            if 'date' in match_data:
                match.date = datetime.datetime.strptime(
                    match_data['date'], '%d.%m.%Y')
            if 'result' in match_data:
                match.result = match_data['result']
            set_match_winner(match)
            match.save()
            if match_data['is_playoff_table'] == 'True':
                fill_next_level_matches(match)
        return HttpResponse(status=200)
