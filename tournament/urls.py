from django.conf.urls import url
from django.contrib.auth.views import logout

from . import views


urlpatterns = [
    url(r'^$', views.TournamentsView.as_view(), name='tournaments'),
    url(r'^logout\?next=(?P<next_page>.*)$', logout, name='logout'),
    url(r'^delete/(?P<id>\d+)$',
        views.TournamentDeleteView.as_view(), name='tournament_delete'),
    url(r'^tournament-create-form$',
        views.TournamentCreateTemplateView.as_view(),
        name='tournament_create_form'),
    url(r'^create$',
        views.TournamentCreateView.as_view(), name='tournament_create'),
    url(r'^tournament/(?P<id>.+)$',
        views.TournamentView.as_view(), name='tournament'),
    url(r'^matches/update/$',
        views.MatchesUpdate.as_view(), name='matches_update'),
]
