# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-22 09:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Schedule',
            new_name='Match',
        ),
        migrations.AlterModelOptions(
            name='match',
            options={'verbose_name': '\u041c\u0430\u0442\u0447', 'verbose_name_plural': '\u041c\u0430\u0442\u0447\u0438'},
        ),
        migrations.AlterField(
            model_name='tournament',
            name='style',
            field=models.IntegerField(verbose_name='\u0422\u0438\u043f \u0442\u0443\u0440\u043d\u0438\u0440\u0430'),
        ),
        migrations.DeleteModel(
            name='TournamentStyle',
        ),
    ]
